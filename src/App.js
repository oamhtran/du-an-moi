
import React, { Components, useState } from 'react'
import Footer from './Components/Layout/Footer';
import Header from './Components/Layout/Header';
import MenuLeft from './Components/Layout/MenuLeft';
import { UserContext } from './UserContext';

function App(props) {
  const [xx, setXx] = useState('')
  function getSumQty(data) {
    setXx(data)
    localStorage.setItem("sluong", data)
  }
  return (
    <>
      <UserContext.Provider value={{
        getSumQty: getSumQty,
        xx: xx
      }}>
        <Header />
        <section>
          <div className="container">
            <div className="row">
              <MenuLeft />
              {props.children}
            </div>
          </div>
        </section>
        <Footer />
      </UserContext.Provider>
    </>
  );
}
export default App;