import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import Home from './Components/Home';
import Checkout from './Components/Product/Checkout';
import Cart from './Components/Product/Cart';
import Detail from './Components/Blog/Detail';
import Index from './Components/Blog/Index';

import Index1 from './Components/Member/Index1';
import Account from './Components/Layout/Account';
import Addproduct from './Components/Member/Addproduct';
import MyProduct from './Components/Product/MyProduct';
import Edita from './Components/Product/Edita';
import ProductDetail from './Components/Product/ProductDeatil';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route index path='/' element={<Home />} />
          <Route path='/checkout' element={<Checkout />} />
          <Route path='/cart' element={<Cart />} />
          <Route path='/blog/detail/:id' element={<Detail />} />
          <Route path='/blog/index' element={<Index />} />
          <Route path='/member/index' element={<Index1 />} />
          <Route path='/member/account' element={<Account />} />
          <Route path='/member/add-product' element={<Addproduct />} />
          <Route path='/member/my-product' element={<MyProduct />} />
          <Route path='/member/edit-product/:id' element={<Edita />} />
          <Route path='/member/product/detail/:id' element={<ProductDetail />} />
        </Routes>
      </App>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
