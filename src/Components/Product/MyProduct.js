import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
// import ReactTable from 'react-table'

function MyProduct() {
    const [xx, setXx] = useState({})
    // const [aa, setAa] = useState("")
    // useEffect chỉ gọi 1 lần vào lúc đầu muốn lấy dữ liệu mới về 
    useEffect(() => {
        const userData = JSON.parse(localStorage["Resdata"])
        // console.log(userData)
        let url = 'http://localhost:8080/laravel/laravel/public/api/user/my-product'
        let accessToken = userData.success.token
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get(url, config)
            .then(res => {
                console.log(res.data.data)
                setXx(res.data.data)

            })
            .catch(error => console.log(error))
    }, [])
    function Delete(e) {
        const id = e.target.id
        // console.log(id)
        const userData = JSON.parse(localStorage["Resdata"])
        let accessToken = userData.success.token
        let url = 'http://localhost:8080/laravel/laravel/public/api/user/delete-product/' + id
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get(url, config)
            .then(res => {
                console.log(res)
                setXx(res.data.data)
            })
            .catch(error => console.log(error))
    }
    // console.log(xx)
    function renderData() {
        if (Object.keys(xx).length > 0) {
            return Object.keys(xx).map((key, index) => {
                const yy = xx[key].image
                // console.log(yy)
                const zz = JSON.parse(yy)
                // console.log(zz[0])
                return (
                    <tr key={key} >
                        <td>{xx[key].id}</td>
                        <td>{xx[key].name}</td>
                        <td>
                            <img className="image-object"
                                src={"http://localhost:8080/laravel/laravel/public/upload/user/product/"
                                    + xx[key].id_user + "/" + zz[0]} alt=""
                            />
                        </td>
                        <td>{xx[key].price}</td>
                        <td>
                            <Link to={"/member/edit-product/" + xx[key].id}>Edit</Link>
                        </td>
                        <td>
                            <a onClick={Delete} id={xx[key].id}>Delete</a>
                        </td>
                    </tr>
                )
            })
        }
    }

    return (
        <>
            <div className="col-sm-4">
                <section id="cart_items">
                    <div className="container">
                        <div className="table-responsive cart_info">
                            <table className="table table-condensed">
                                <thead>
                                    <tr className="cart_menu">
                                        <td className="id">Id</td>
                                        <td className="name" >Name</td>
                                        <td className="image">Image</td>
                                        <td className="price">Price</td>
                                        <td className="action" colSpan={2}>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {renderData()}
                                </tbody>
                            </table>

                        </div>
                        <button className="button" type="button">< Link to="/member/add-product">Add New</Link></button>
                    </div>
                </section>
            </div >

        </>
    )

}
export default MyProduct;