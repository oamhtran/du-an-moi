import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import FormError from "../FormError";

function Edit(props) {
    const [input, setInput] = useState({
        name: "",
        price: "",
        category: "",
        brand: "",
        status: 1,
        sale: "",
        detail: "",
        company_profile: "",
        image: []

    })
    const [errors, setErrors] = useState({})
    const [category, setCategory] = useState("")
    const [brand, setBrand] = useState("")
    const [getFile, setFile] = useState({})

    function handleInput(e) {
        const nameInput = e.target.name
        const value = e.target.value
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    let params = useParams()
    useEffect(() => {
        const userData = JSON.parse(localStorage["Resdata"])
        let accessToken = userData.success.token
        let url = 'http://localhost:8080/laravel/laravel/public/api/user/product/' + params.id
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        // console.log(url)
        axios.get(url, config)
            .then(resp => {
                let xx = (resp.data.data)
                console.log(xx)

                setInput({
                    name: xx.name,
                    price: xx.price,
                    category: xx.category,
                    brand: xx.brand,
                    status: xx.status,
                    sale: xx.sale,
                    detail: xx.detail,
                    company_profile: xx.company_profile,
                    image: xx.image,
                    id_user: xx.id_user
                })

            })
            .catch(error => console.log(error))

        axios.get("http://localhost:8080/laravel/laravel/public/api/category-brand")
            .then(res => {
                // console.log(res)
                setCategory(res.data.category)
                setBrand(res.data.brand)
            })
            .catch(error => console.log(error))
    }, [])

    function renderCategory() {
        if (category.length > 0) {
            return category.map((value, index) => {
                return (
                    <option value={value.id}>{value.category}</option>
                )
            })
        }
    }
    const [avatarCheckBox, setavatarCheckBox] = useState([])
    function imgCheckbox(e) {
        const name = e.target.name
        const avatar = []
        avatar.push(name);
        // console.log(avatar)
        setavatarCheckBox(avatar)
    }
    console.log(avatarCheckBox)
    function renderImg() {
        let xx = input.image
        console.log(xx)
        if (xx.length > 0) {
            return xx.map((value, index) => {
                return (
                    <li>
                        <img className="img_edit" src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" +
                            input.id_user + "/" + value} />
                        <input type="checkbox" name={value} onClick={imgCheckbox} />
                    </li>
                )
            })
        }
    }
    function renderBrand() {
        if (brand.length > 0) {
            return brand.map((value, index) => {
                return (
                    <option value={value.id}>{value.brand}</option>
                )
            })
        }
    }
    function renderSale() {
        if (input.status == 0) {
            return (
                <input name="sale" value={input.sale} onChange={handleInput}></input>
            )
        }
    }
    function handleInputfiles(e) {
        setFile(e.target.files)
    }
    function handleSubmit(e) {
        e.preventDefault()
        let errorSubmit = {}
        let flag = true
        if (input.name == "") {
            errorSubmit.name = "Vui long nhap ten "
            flag = false
        }
        if (input.price == "") {
            errorSubmit.price = "Vui loong nhap gia"
            flag = false
        }
        if (input.category == "") {
            errorSubmit.category = "Vui long chon category"
            flag = false
        }
        if (input.brand == "") {
            errorSubmit.brand = " Vui long chon brand"
            flag = false
        }

        if (getFile == "") {
            errorSubmit.getFile = "Vui long chon anh"
            flag = false
        } else {
            if (getFile.length > 3) {
                errorSubmit.getFile = "Chỉ được chọn tối đa 3 ảnh "
                flag = false
            } else {
                Object.keys(getFile).map((key, value) => {
                    if (getFile[key].size > 1024 * 1024) {
                        errorSubmit.getFile = "Dung lượng file ảnh quá lớn.Vui lòng chọn lại ảnh "
                        flag = false
                    } else {
                        const nameimg = getFile[key].name.split(".")[1]
                        const type = ["png", "jpg", "jpeg", "PNG", "JPG"]
                        if (!type.includes(nameimg)) {
                            errorSubmit.getFile = " Định dạng file ảnh không hợp lệ. Vui lòng chọn lại ảnh"
                            flag = false
                        }
                    }
                })
            }
        }
        if (!flag) {
            setErrors(errorSubmit)
        } else {
            setErrors({})
            const userData = JSON.parse(localStorage["Resdata"])
            let accessToken = userData.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            // console.log(input.status)
            let url = 'http://localhost:8080/laravel/laravel/public/api/user/edit-product/' + params.id
            const formData = new FormData()
            formData.append('name', input.name)
            formData.append('price', input.price)
            formData.append('category', input.category)
            formData.append('company', input.company_profile)
            formData.append('brand', input.brand)
            formData.append('detail', input.detail)
            formData.append('status', input.status)
            formData.append('sale', input.sale ? input.sale : 0)

            Object.keys(avatarCheckBox).map((item, i) => {
                formData.append("avatarCheckBox[]", avatarCheckBox[item])
            })
            Object.keys(getFile).map((item, i) => {
                formData.append("file[]", getFile[item])
            })
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res)
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                        alert(res.data.errors)
                    } else {
                        alert("Edit thanh cong")
                    }
                })
                .catch(error => console.log(error))
        }

    }
    return (
        <>
            <FormError errors={errors} />
            <div className="col-sm-4">
                <div className="signup-form">
                    <form onSubmit={handleSubmit} >
                        <input type="text" name="name" value={input.name} onChange={handleInput} />
                        <input type="text" name="price" value={input.price} onChange={handleInput} />
                        <select name="category" value={input.category} onChange={handleInput}>
                            <option value="">Please choose category</option>
                            {renderCategory()}
                        </select>
                        <select name="brand" value={input.brand} onChange={handleInput}>
                            <option value="">Please choose brand</option>
                            {renderBrand()}
                        </select>
                        <select name="status" onChange={handleInput}>

                            <option value={1}>New</option>
                            <option value={0}>Sale</option>
                        </select>
                        {renderSale()}
                        <input name="company_profile" value={input.company_profile} onChange={handleInput} />
                        <input name="files" id="files" multiple type="file" onChange={handleInputfiles}></input>

                        <ul>
                            {renderImg()}
                        </ul>
                        <textarea name="detail" value={input.detail} onChange={handleInput}></textarea>
                        <button type="submit">Signup</button>
                    </form>
                </div>
            </div>
        </>
    )
}
export default Edit;




// import axios from "axios"
// import { useEffect, useState } from "react"
// import FormError from "../FormError"

// function Edit(props) {
//     const [xx, setXx] = useState("")
//     const [input, setInput] = useState({
//         name: "",
//         price: "",
//         category: "",
//         brand: "",
//         status: "",
//         sale: ""
//     })
//     const [brand, setBrand] = useState({})
//     const [category, setCategory] = useState({})
//     function handleInput(e) {
//         const nameInput = e.target.name
//         const value = e.target.value
//         setInput(state => ({ ...state, [nameInput]: value }))
//     }

//     const [errors, setErrors] = useState({})
//     useEffect(() => {
//         axios.get("http://localhost:8080/laravel/laravel/public/api/category-brand")
//             .then(Response => {
//                 setBrand(Response.data.brand)
//                 setCategory(Response.data.category)
//             })
//             .catch(error => console.log(error))

//         const userData = JSON.parse(localStorage["Resdata"])
//         let accessToken = userData.success.token
//         let url = 'http://localhost:8080/laravel/laravel/public/api/user/my-product'
//         let config = {
//             headers: {
//                 'Authorization': 'Bearer ' + accessToken,
//                 'Content-Type': 'application/x-www-form-urlencoded',
//                 'Accept': 'application/json'
//             }
//         };
//         axios.get(url, config)
//             .then(res => {
//                 console.log(res.data.data)
//                 setXx(res.data.data)
//             })
//             // console.log(xx)
//             .catch(error => console.log(error))
//         Object.keys(xx).map((key, index) => {
//             setInput({
//                 name: xx[key].name,
//                 price: xx[key].price,
//                 category: xx[key].id_category,
//                 brand: xx[key].id_brand,
//                 status: xx[key].id_status,
//                 sale: xx[key].sale
//             })
//         })

//     }, [])

//     function renderCategory() {
//         if (category.length > 0)
//             return category.map((value, index) => {
//                 return (
//                     <>
//                         <option key={index} value={value.id}>{value.category}</option>
//                     </>
//                 )
//             })
//     }
//     function renderBrand() {
//         if (brand.length > 0)
//             return brand.map((value, index) => {
//                 return (
//                     <>
//                         <option key={index} value={value.id}>{value.brand}</option>
//                     </>
//                 )
//             })
//     }
//     function renderSale() {
//         if (input.status == "0") {
//             return (
//                 <input name="sale" onChange={handleInput} ></input>
//             )
//         }
//     }
//     return (
//         <>
//             <>
//                 <FormError errors={errors} />
//                 <div className="col-sm-4">
//                     <div className="signup-form">
//                         <form >
//                             <input type="text" name="name" value={input.name} onChange={handleInput} />
//                             <input type="text" name="price" value={input.price} onChange={handleInput} />
//                             <select name="category" value={input.category} onChange={handleInput}>
//                                 {renderCategory()}
//                             </select>
//                             <select name="brand" onChange={handleInput}>
//                                 {renderBrand()}
//                             </select>
//                             <select name="status" onChange={handleInput}>
//                                 <option value={1}>new</option>
//                                 <option value={0}>sale</option>
//                             </select>
//                             {renderSale()}
//                             <input type="text" name="sale" value={input.sale} onChange={handleInput}></input>
//                             <button type="submit">Signup</button>
//                         </form>
//                     </div>

//                 </div>
//             </>
//         </>
//     )
// }
// export default Edit