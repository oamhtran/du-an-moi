import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import FormError from "../FormError"


function Edita() {
    const [input, setInput] = useState({
        name: "",
        price: "",
        category: "",
        brand: "",
        status: 1,
        sale: "",
        company_profile: "",
        detail: "",
        image: [],

    })
    const [category, setCategory] = useState("")
    const [brand, setBrand] = useState("")
    const [errors, setErrors] = useState({})
    let params = useParams()
    const [getFile, setFile] = useState({})
    const [avatarCheckBox, setAvatarCheckBox] = useState([])
    function handleInput(e) {
        const nameInput = e.target.name
        const value = e.target.value
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/laravel/public/api/category-brand")
            .then(res => {
                // console.log(res)
                setCategory(res.data.category)
                setBrand(res.data.brand)
            })
            .catch(error => console.log(error))
        const userData = JSON.parse(localStorage["Resdata"])
        let accessToken = userData.success.token
        let url = 'http://localhost:8080/laravel/laravel/public/api/user/product/' + params.id
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        };
        axios.get(url, config)
            .then(resp => {
                // console.log(resp)
                let xx = resp.data.data
                console.log(xx)
                setInput({
                    name: xx.name,
                    price: xx.price,
                    category: xx.id_category,
                    brand: xx.id_brand,
                    status: xx.status,
                    sale: xx.sale,
                    company_profile: xx.company_profile,
                    detail: xx.detail,
                    image: xx.image,
                    id_user: xx.id_user

                })
            })
    }, [])

    function renderCategory() {
        if (category.length > 0) {
            return category.map((value, index) => {
                return (
                    <option value={value.id}>{value.category}</option>
                )
            })
        }
    }
    function renderBrand() {
        if (brand.length > 0) {
            return brand.map((value, key) => {
                return (
                    <option value={value.id}>{value.brand}</option>
                )
            })
        }
    }
    function renderSale() {
        if (input.status == 0) {
            return (
                <input type="text" name="sale" onChange={handleInput} />
            )
        }
    }
    function handleInputFiles(e) {
        setFile(e.target.files)
    }

    function renderImg() {
        let yy = input.image
        console.log(yy)
        if (yy.length > 0) {
            return yy.map((value, index) => {
                // console.log(value)
                return (
                    <li>
                        <img className="img_id" src={"http://localhost:8080/laravel/laravel/public/upload/user/product/" +
                            input.id_user + "/" + value} />
                        <input type="checkbox" name={value} onClick={imgCheckbox} />

                    </li>
                )
            })
        }
    }
    function imgCheckbox(e) {
        const name = e.target.name
        const avatar = []
        avatar.push(name)
        setAvatarCheckBox(avatar)
    }
    console.log(avatarCheckBox)
    function handleSubmit(e) {
        let errorSubmit = {}
        let flag = true
        e.preventDefault()

        if (input.name == "") {
            errorSubmit.name = "Vui long nhap ten"
            flag = false
        };
        if (input.price == "") {
            errorSubmit.price = "Vui long nhap gia"
            flag = false
        }

        if (input.category == "") {
            errorSubmit.category = "Vui long chon category"
            flag = false
        }
        if (input.brand == "") {
            errorSubmit.brand = "Vui long chon brand"
            flag = false
        }

        if (input.company_profile == "") {
            errorSubmit.company_profile = "Vui long nhap thong tin cong ty"
        }
        if (input.detail == "") {
            errorSubmit.detail = "Vui long nhap thong tin chi tiet san pham"
        }
        if (getFile == "") {
            errorSubmit.getFile = "Vui long chon anh"
            flag = false
        } else {
            if (getFile.length > 3) {
                errorSubmit.getFile = "Chỉ được chọn tối đa 3 ảnh "
                flag = false
            } else {
                Object.keys(getFile).map((key, value) => {
                    if (getFile[key].size > 1024 * 1024) {
                        errorSubmit.getFile = "Dung lượng file ảnh quá lớn.Vui lòng chọn lại ảnh "
                        flag = false
                    } else {
                        const nameimg = getFile[key].name.split(".")[1]
                        const type = ["png", "jpg", "jpeg", "PNG", "JPG"]
                        if (!type.includes(nameimg)) {
                            errorSubmit.getFile = " Định dạng file ảnh không hợp lệ. Vui lòng chọn lại ảnh"
                            flag = false
                        }
                    }
                })
            }
        }

        if (!flag) {
            setErrors(errorSubmit)
        } else {
            setErrors({})
            const userData = JSON.parse(localStorage["Resdata"])
            let accessToken = userData.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            // console.log(input.status)
            let url = 'http://localhost:8080/laravel/laravel/public/api/user/edit-product/' + params.id
            const formData = new FormData()
            formData.append('name', input.name)
            formData.append('price', input.price)
            formData.append('category', input.category)
            formData.append('company', input.company_profile)
            formData.append('brand', input.brand)
            formData.append('detail', input.detail)
            formData.append('status', input.status)
            formData.append('sale', input.sale ? input.sale : 0)

            Object.keys(avatarCheckBox).map((item, i) => {
                formData.append("avatarCheckBox[]", avatarCheckBox[item])
            })
            Object.keys(getFile).map((item, i) => {
                formData.append("file[]", getFile[item])
            })
            console.log(getFile)
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res)
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                        alert(res.data.errors)
                    } else {
                        alert("Edit thanh cong")
                    }
                })
                .catch(error => console.log(error))
        }
    }
    return (
        <>
            <FormError errors={errors} />
            <div className="col-sm-4">
                <div className="signup-form">
                    <form onSubmit={handleSubmit} >
                        <input type="text" name="name" value={input.name} onChange={handleInput} />
                        <input type="text" name="price" value={input.price} onChange={handleInput} />

                        <select name="category" value={input.category} onChange={handleInput}>
                            <option value="">Please choose category</option>
                            {renderCategory()}
                        </select>
                        <select name="brand" value={input.brand} onChange={handleInput}>
                            <option value="">Please choose brand</option>
                            {renderBrand()}
                        </select>
                        <select name="status" value={input.status} onChange={handleInput}>

                            <option value={1}>New</option>
                            <option value={0}>Sale</option>
                        </select>
                        {renderSale()}
                        <input type="text" name="company_profile" value={input.company_profile} onChange={handleInput} />
                        <input type="file" name="files" id="files" multiple onChange={handleInputFiles} />
                        <ul>
                            {renderImg()}
                        </ul>
                        <textarea type="text" name="detail" value={input.detail} onChange={handleInput} />
                        <button type="submit">Signup</button>
                    </form>
                </div>
            </div>
        </>
    )
}
export default Edita
