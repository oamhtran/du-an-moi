import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../../UserContext";

function Cart() {

    const user = useContext(UserContext)
    const [xx, setXx] = useState({})
    useEffect(() => {
        const add = localStorage.getItem("addtocard")
        if (add) {
            const zz = JSON.parse(add)
            axios.post("http://localhost:8080/laravel/laravel/public/api/product/cart", zz)
                .then((res) => {
                    // console.log(res)
                    setXx(res.data.data)
                })
        }
    }, [])
    // console.log(xx)
    function cartQuantityUp(e) {
        let qty = 0
        const getId = e.target.id
        // copy ra 1 cai moi 
        let yy = [...xx]
        yy.map((value, key) => {
            if (getId == value.id) {
                yy[key]['qty'] += 1;
            }
            qty = qty + yy[key]['qty']
        })
        user.getSumQty(qty)
        setXx(yy)
        // khi nao chay html moi chay retur

        // thay doi trong ilocal
        const add = localStorage.getItem("addtocard")
        if (add) {
            const zz = JSON.parse(add)
            if (Object.keys.length > 0) {
                Object.keys(zz).map((key, index) => {
                    if (getId == key) {
                        zz[key] = zz[key] + 1
                        localStorage.setItem("addtocard", JSON.stringify(zz))
                    }
                })
            }
        }
    }
    function cartQuantityDown(e) {
        let qty = 0
        const getId = e.target.id
        let getQty = e.target.name
        getQty = getQty - 1
        const add = localStorage.getItem("addtocard")
        const zz = JSON.parse(add)

        if (getQty < 1) {
            let qty = 0

            let yy = [...xx]
            yy.map((value, key) => {
                if (getId == value.id) {
                    delete yy[key]
                }


            })

            yy = yy.filter(function (v) { return v !== '' });
            yy.map((value, key) => {
                qty = qty + yy[key]['qty']

            })

            user.getSumQty(qty)

            setXx(yy)


            if (add) {
                if (Object.keys.length > 0) {
                    Object.keys(zz).map((key, value) => {
                        if (getId == key) {
                            delete zz[key]
                        }
                    })
                }
            }

        } else {
            let yy = [...xx]
            yy.map((value, key) => {
                if (getId == value.id) {
                    yy[key]['qty'] -= 1;
                }
                qty = qty + yy[key]['qty']
            })
            user.getSumQty(qty)
            setXx(yy)
            // thay doi trong ilocal
            if (add) {
                if (Object.keys.length > 0) {
                    Object.keys(zz).map((key, index) => {
                        if (getId == key) {
                            zz[key] = zz[key] - 1
                        }
                        // qty = qty + zz[key]
                    })
                }
            }
            // user.getSumQty(qty)
        }
        localStorage.setItem("addtocard", JSON.stringify(zz))
    }
    function cartQuantityDelete(e) {
        let qty = 0
        const getId = e.target.id
        const add = localStorage.getItem("addtocard")
        let yy = [...xx]
        yy.map((value, key) => {
            if (getId == value.id) {
                delete yy[key]
            }
        })
        yy = yy.filter(function (v) { return v !== '' });
        yy.map((value, key) => {
            qty = qty + yy[key]['qty']
        })
        user.getSumQty(qty)
        setXx(yy)
        if (add) {
            const zz = JSON.parse(add)
            if (Object.keys.length > 0) {
                Object.keys(zz).map((key, value) => {
                    if (getId == key) {
                        delete zz[key]
                        localStorage.setItem("addtocard", JSON.stringify(zz))

                    }

                })
            }
        }
    }
    // thay doi tren local vaf man hinh
    // tren man hinh: ktra xem id trong xx co ko
    // local ktra id xem co ko

    function renderData() {
        if (xx.length > 0) {
            return xx.map((value, index) => {
                const zz = JSON.parse(value.image)
                const sum = value.price * value.qty
                return (
                    <tr>
                        <td className="cart_product">
                            <a href><img className="image-object" src={"http://localhost:8080/laravel/laravel/public/upload/user/product/"
                                + value.id_user + "/" + zz[0]} alt="" /></a>
                        </td>
                        <td className="cart_description">
                            <h4><a href>{value.name}</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td className="cart_price">
                            <p>{value.price}</p>
                        </td>
                        <td className="cart_quantity">
                            <div className="cart_quantity_button">
                                <a onClick={cartQuantityUp} id={value.id} className="cart_quantity_up" href> + </a>
                                <input className="cart_quantity_input" type="text"
                                    name="quantity" value={value.qty} autoComplete="off" size={2} />

                                <a onClick={cartQuantityDown} id={value.id} name={value.qty} className="cart_quantity_down" href> - </a>
                            </div>
                        </td>
                        <td className="cart_total">
                            <p className="cart_total_price" >{sum}</p>
                        </td>
                        <td className="cart_delete">
                            <a onClick={cartQuantityDelete} id={value.id} className="cart_quantity_delete" >xoa</a>
                        </td>
                    </tr >
                )
            })

        }
    }
    function renderTotal() {
        let tong = 0
        if (xx.length > 0) {
            xx.map((value, index) => {
                let sum = value.price * value.qty
                tong = tong + sum;
            })
        }
        return tong;
    }
    localStorage.setItem("muahang", JSON.stringify(xx))

    return (
        <div>
            <section id="cart_items">
                <div className="container">
                    <div className="breadcrumbs">
                        <ol className="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li className="active">Shopping Cart</li>
                        </ol>
                    </div>
                    <div className="table-responsive cart_info">
                        <table className="table table-condensed">
                            <thead>
                                <tr className="cart_menu">
                                    <td className="image">Item</td>
                                    <td className="description" />
                                    <td className="price">Price</td>
                                    <td className="quantity">Quantity</td>
                                    <td className="total">Total</td>
                                    <td />
                                </tr>
                            </thead>
                            <tbody>
                                {renderData()}

                            </tbody>
                        </table>
                    </div>
                </div>
            </section> {/*/#cart_items*/}
            <section id="do_action">
                <div className="container">
                    <div className="heading">
                        <h3>What would you like to do next?</h3>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="chose_area">
                                <ul className="user_option">
                                    <li>
                                        <input type="checkbox" />
                                        <label>Use Coupon Code</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" />
                                        <label>Use Gift Voucher</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" />
                                        <label>Estimate Shipping &amp; Taxes</label>
                                    </li>
                                </ul>
                                <ul className="user_info">
                                    <li className="single_field">
                                        <label>Country:</label>
                                        <select>
                                            <option>United States</option>
                                            <option>Bangladesh</option>
                                            <option>UK</option>
                                            <option>India</option>
                                            <option>Pakistan</option>
                                            <option>Ucrane</option>
                                            <option>Canada</option>
                                            <option>Dubai</option>
                                        </select>
                                    </li>
                                    <li className="single_field">
                                        <label>Region / State:</label>
                                        <select>
                                            <option>Select</option>
                                            <option>Dhaka</option>
                                            <option>London</option>
                                            <option>Dillih</option>
                                            <option>Lahore</option>
                                            <option>Alaska</option>
                                            <option>Canada</option>
                                            <option>Dubai</option>
                                        </select>
                                    </li>
                                    <li className="single_field zip-field">
                                        <label>Zip Code:</label>
                                        <input type="text" />
                                    </li>
                                </ul>
                                <a className="btn btn-default update" href>Get Quotes</a>
                                <a className="btn btn-default check_out" href>Continue</a>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="total_area">
                                <ul>
                                    <li>Cart Sub Total <span>$59</span></li>
                                    <li>Eco Tax <span>$2</span></li>
                                    <li>Shipping Cost <span>Free</span></li>
                                    <li>Total <span className="total" />{renderTotal()}</li>
                                </ul>
                                <a className="btn btn-default update" href>Update</a>
                                <a className="btn btn-default check_out" href>Check Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>{/*/#do_action*/}
        </div>
    )
}
export default Cart;