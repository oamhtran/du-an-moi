import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import FormError from "../FormError";

function Login() {
    const [input, setInput] = useState({
        email: "",
        password: ""
    })
    const navigate = useNavigate()
    const [errors, setErrors] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        let errorSubmit = {}
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let flag = true;

        if (input.email == "") {
            flag = false;
            errorSubmit.email = "Vui lòng nhập email"
        } else if (!regex.test(input.email)) {
            flag = false;
            errorSubmit.email = "Email sai"
        }
        if (input.password == "") {
            flag = false;
            errorSubmit.password = "Vui long nhap mat khau"
        }
        if (!flag) {
            setErrors(errorSubmit)
        } else {
            setErrors({})
            const data = {
                email: input.email,
                password: input.password,
                level: 0
            }
            axios.post("http://localhost:8080/laravel/laravel/public/api/login", data)
                .then((res) => {
                    // console.log(res)
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                    }
                    else {
                        navigate('/')
                        alert("Đăng nhập thành công")
                        let xx = true
                        localStorage.setItem("login", JSON.stringify(xx))
                        const yy = res.data
                        localStorage.setItem("Resdata", JSON.stringify(yy))
                    }
                })
        }
    }
    return (
        <>
            <FormError errors={errors} />
            <div className="col-sm-4 col-sm-offset-1">
                <div className="login-form">
                    <h2>Login to your account</h2>
                    <form onSubmit={handleSubmit}  >
                        <input type="text" placeholder="Email" name="email" onChange={handleInput} />
                        <input type="password" placeholder="Password" name="password" onChange={handleInput} />
                        <select name="level">
                            <option>0</option>
                        </select>
                        <span>
                            <input type="checkbox" className="checkbox" />
                            Keep me signed in
                        </span>
                        <button type="submit" className="btn btn-default">Login</button>
                    </form>
                </div>
            </div>
        </>
    )
}
export default Login;
