
import { useState } from "react";
import FormError from "../FormError";
import axios from "axios"
function Register() {
    const [input, setInput] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: ""
    })
    const [errors, setErrors] = useState({})
    const [getFile, setFile] = useState("")
    const [avatar, setAvarta] = useState("")

    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    // Day la phan chia file anh thanh 2 phan: 1 dua ve API, 2 dua ve xu li anh
    function handleUserInputFile(e) {
        const file = e.target.files
        let reader = new FileReader()
        reader.onload = (e) => {
            setAvarta(e.target.result)
            setFile(file)
        }
        reader.readAsDataURL(file[0])
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let errorSubmit = {}
        let flag = true;
        if (input.name == "") {
            flag = false;
            errorSubmit.name = "Vui lòng nhập tên"
        }
        if (input.email == "") {
            flag = false;
            errorSubmit.email = "Vui lòng nhập email"
        } else if (!regex.test(input.email)) {
            flag = false;
            errorSubmit.email = "Email sai"
        }
        if (input.password == "") {
            flag = false;
            errorSubmit.pass = "Vui lòng nhập mật khẩu"
        }
        if (input.phone == "") {
            flag = false;
            errorSubmit.phone = "Vui lòng nhập số điện thoại"
        }
        if (input.address == "") {
            flag = false;
            errorSubmit.address = "Vui lòng nhập địa chỉ"
        }
        if (getFile == "") {
            flag = false;
            errorSubmit.avarta = "Vui lòng chọn ảnh"
        } else {
            if (getFile[0].size > 1024 * 1024) {
                errorSubmit.avarta = "Dung lượng file ảnh lớn.Vui lòng chọn lại ảnh"
                flag = false
            } else {
                const nameimg = getFile[0].type.replace("image/", "")
                const type = ["png", "jpg", "jpeg", "PNG", "JPG"]
                if (!type.includes(nameimg)) {
                    errorSubmit.avarta = "Định dạng file ảnh không hợp lệ"
                    flag = false
                }
            }
        }
        if (!flag) {
            setErrors(errorSubmit)
        } else {
            setErrors({})
            const data = {
                name: input.name,
                email: input.email,
                password: input.password,
                phone: input.phone,
                avatar: avatar,
                address: input.address,
                level: 0
            }
            axios.post("http://localhost:8080/laravel/laravel/public/api/register", data)
                .then((res) => {
                    console.log(res)
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                    } else {
                        alert("dk thanh cong")
                    }
                })
        }
    }

    return (
        <>
            <FormError errors={errors} />
            <div className="col-sm-4">
                <div className="signup-form">{/*sign up form*/}
                    <h2>New User Signup!</h2>
                    <form onSubmit={handleSubmit}>
                        <input type="text" placeholder="Name" name="name" onChange={handleInput} />
                        <input type="email" placeholder="Email Address" name="email" onChange={handleInput} />
                        <input type="password" placeholder="Password" name="password" onChange={handleInput} />
                        <input type="text" placeholder="Phone" name="phone" onChange={handleInput} />
                        <input type="text" placeholder="Address" name="address" onChange={handleInput} />
                        <input type="file" name="avarta" onChange={handleUserInputFile}  ></input>
                        <select placeholder="Level" name="level"  >
                            <option>0</option>
                        </select>
                        <button type="submit">Signup</button>
                        {/* className="btn btn-default" */}
                    </form>
                </div>{/*/sign up form*/}
            </div>
        </>
    )
}
export default Register;