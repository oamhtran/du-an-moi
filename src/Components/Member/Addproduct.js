import axios from "axios";
import { useEffect, useState } from "react";
import FormError from "../FormError";

function Addproduct() {
    const [input, setInput] = useState({
        name: "",
        price: "",
        company_profile: "",
        detail: "",
        brand: "",
        status: 1,
        category: "",
        sale: ""
    })

    const [errors, setErrors] = useState({})
    const [getFile, setFile] = useState("")

    const [brand, setBrand] = useState({})
    const [category, setCategory] = useState({})
    const handleInput = (e) => {
        const nameInput = e.target.name
        const value = e.target.value
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    function handleUserInputFile(e) {
        setFile(e.target.files);
    }
    // console.log(getFile)
    const handleSubmit = (e) => {
        e.preventDefault();

        let errorSubmit = {}
        let flag = true;
        if (input.name == "") {
            flag = false;
            errorSubmit.email = "Vui lòng nhập tên"
        }
        if (input.price == "") {
            flag = false;
            errorSubmit.price = "Vui lòng nhập giá sản phẩm"
        }
        if (input.brand == "") {
            flag = false
            errorSubmit.brand = "Vui lòng chọn brand"
        }
        if (input.category == "") {
            flag = false
            errorSubmit.category = "Vui lòng chọn category"
        }

        if (input.company_profile == "") {
            flag = false;
            errorSubmit.company_profile = "Vui lòng nhập thong tin công ty"
        }
        if (input.detail == "") {
            flag = false;
            errorSubmit.detail = "Vui lòng nhập chi tiết sản phẩm"
        }
        if (getFile == "") {
            flag = false;
            errorSubmit.getFile = "Vui lòng chọn ảnh"
        } else {
            if (getFile.length > 3) {
                errorSubmit.getFile = "Chỉ được chọn tối đa 3 ảnh"
                flag = false
            } else {
                Object.keys(getFile).map((key, index) => {
                    if (getFile[key].size > 1024 * 1024) {
                        errorSubmit.getFile = "Dung lượng file ảnh lớn.Vui lòng chọn lại ảnh"
                        flag = false
                    } else {
                        const nameimg = getFile[key].name.split(".")[1]
                        console.log(nameimg)
                        const type = ["png", "jpg", "jpeg", "PNG", "JPG"]
                        if (!type.includes(nameimg)) {
                            errorSubmit.getFile = "Định dạng file ảnh không hợp lệ"
                            flag = false
                        }
                    }
                })
            }
        }

        if (!flag) {
            setErrors(errorSubmit)
        } else {
            setErrors({})
            const userData = JSON.parse(localStorage["Resdata"])

            let url = 'http://localhost:8080/laravel/laravel/public/api/user/add-product'
            let accessToken = userData.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData()
            formData.append('name', input.name)
            formData.append('price', input.price)
            formData.append('category', input.category)
            formData.append('brand', input.brand)
            formData.append('company', input.company_profile)
            formData.append('detail', input.detail)
            formData.append('status', input.status)
            formData.append('sale', input.sale ? input.sale : 0)

            Object.keys(getFile).map((item, i) => {
                formData.append("file[]", getFile[item])
            })
            axios.post(url, formData, config)
                .then(res => {
                    console.log(res)
                    if (res.data.errors) {
                        setErrors(res.data.errors)
                    } else {
                        alert("Add thanh cong")
                    }
                })
                .catch(error => console.log(error))
        }
    }
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/laravel/public/api/category-brand")
            .then(res => {
                // console.log(res)
                setBrand(res.data.brand)
                setCategory(res.data.category)
            })
            .catch(error => console.log(error))
    }, [])

    function renderBrand() {
        if (brand.length > 0)
            return brand.map((value, index) => {
                return (
                    <>
                        <option key={index} value={value.id}>{value.brand}</option>
                    </>
                )
            })
    }
    function renderCategory() {
        if (category.length > 0)
            return category.map((value, index) => {
                return (
                    <>
                        <option key={index} value={value.id}>{value.category}</option>
                    </>
                )
            })
    }
    function renderSale() {
        if (input.status == "0") {
            return (
                <input name="sale" onChange={handleInput} ></input>
            )
        }
    }
    return (
        <>
            <FormError errors={errors} />
            <div className="col-sm-4">
                <div className="signup-form">
                    <h2>Creat Product!</h2>
                    <form onSubmit={handleSubmit}>
                        <input type="text" placeholder="Name" name="name" onChange={handleInput} />
                        <input type="price" placeholder="Price" name="price" onChange={handleInput} />
                        <select name="brand" onChange={handleInput}>
                            <option >Please choose brand</option>
                            {renderBrand()}
                        </select>
                        <select name="category" onChange={handleInput}>
                            <option >Please choose category</option>
                            {renderCategory()}
                        </select>
                        <select name="status" onChange={handleInput}>
                            <option value={1}>new</option>
                            <option value={0}>sale</option>
                        </select>
                        {renderSale()}
                        <input placeholder="Company profile" name="company_profile" onChange={handleInput} />
                        <input type="file" id="files" name="files" multiple onChange={handleUserInputFile} />
                        <textarea type="detail" placeholder="Detail" name="detail" onChange={handleInput} />
                        <button type="submit" className="btn btn-default">Signup</button>
                    </form>
                </div>

            </div>
        </>

    )
}
export default Addproduct;
