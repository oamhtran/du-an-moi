import axios from "axios";
import { useEffect, useState } from "react";
import FormError from "../FormError";

function Account() {
    const [input, setInput] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: ""
    })
    const [avatar, setAvatar] = useState("")
    const [errors, setErrors] = useState({})
    const [getFile, setFile] = useState("")
    const userData = JSON.parse(localStorage["Resdata"])

    useEffect(() => {
        const userData = JSON.parse(localStorage["Resdata"])
        setInput({
            name: userData.Auth.name,
            email: userData.Auth.email,
            password: userData.Auth.password,
            phone: userData.Auth.phone,
            address: userData.Auth.address
        })
    }, [])
    function handleInput(e) {
        const nameInput = e.target.name
        const value = e.target.value
        setInput(state => ({ ...state, [nameInput]: value }))
    }
    function handleUserInputFile(e) {
        const file = e.target.files
        let reader = new FileReader()
        reader.onload = (e) => {
            setAvatar(e.target.result)
            setFile(file)
        }
        reader.readAsDataURL(file[0])
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        let errorSubmit = {}
        let flag = true;

        if (getFile != "") {
            if (getFile[0].size
                > 1024 * 1024) {
                errorSubmit.avatar = "Dung lượng file ảnh lớn.Vui lòng chọn lại ảnh"
                flag = false
            } else {
                const nameimg = getFile[0].name.split(".")[1]
                const type = ["png", "jpg", "jpeg", "PNG", "JPG"]
                if (!type.includes(nameimg)) {
                    errorSubmit.avatar = "Định dạng file ảnh không hợp lệ"
                    flag = false
                }
            }
        }
        if (!flag) {
            setErrors(errorSubmit)
        } else {
            setErrors({})
            let url = 'http://localhost:8080/laravel/laravel/public/api/user/update/' + userData.Auth.id
            let accessToken = userData.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData()
            formData.append('name', input.name)
            formData.append('email', input.email)
            formData.append('password', input.password)
            formData.append('phone', input.phone)
            if (getFile != "") {
                formData.append('avatar', avatar)
            }
            formData.append('address', input.address)

            axios.post(url, formData, config)
                .then(res => {
                    console.log(res)
                    if (res.data.errors) {
                        alert(res.data.errors)
                    } else {
                        alert("update thanh cong")
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }
    return (
        <>
            <FormError errors={errors} />
            <div className="col-sm-4">
                <div className="signup-form">
                    {/*sign up form*/}
                    <h2> User Update!</h2>
                    <form onSubmit={handleSubmit} >
                        <input type="text" name="name" value={input.name} onChange={handleInput} />
                        <input type="text" name="email" value={input.email} readOnly />
                        <input type="text" name="password" value={input.password} onChange={handleInput} />
                        <input type="text" name="address" value={input.address} onChange={handleInput} />
                        <input type="text" name="phone" value={input.phone} onChange={handleInput} />
                        <input type="file" name="avarta" onChange={handleUserInputFile}  ></input>
                        <button type="submit">Signup</button>
                    </form>
                </div>
                {/*/sign up form*/}
            </div>
        </>

    )
}
export default Account;
