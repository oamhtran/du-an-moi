
function ListComment(props) {
    const xx = props.data
    function Replay(e) {
        const id = e.target.id
        props.getId(id)
    }

    function renderData(e) {
        if (xx.length > 0) {
            return xx.map((value, key) => {
                if (value.id_comment == 0) {
                    return (
                        <>
                            <li key={key} className="media">
                                <a className="pull-left" href="#">
                                    <img className="media-object"
                                        src={"http://localhost:8080/laravel/laravel/public/upload/user/avatar/" +
                                            value.image_user} alt=""
                                    />
                                </a>
                                <div className="media-body">
                                    <ul className="sinlge-post-meta">
                                        <li><i className="fa fa-user" />Janis Gallagher</li>
                                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                    </ul>
                                    <p>{value.comment}</p>
                                    <a onClick={Replay} href="#idrelp" className="btn btn-primary"
                                        id={value.id}><i className="fa fa-reply" />Replay</a>
                                </div>
                            </li>
                            {
                                xx.map((value2, key2) => {
                                    if (value.id == value2.id_comment) {
                                        return (
                                            <li key={key2} className="media second-media">
                                                <a className="pull-left" href="#">
                                                    <img className="media-object"
                                                        src={"http://localhost:8080/laravel/laravel/public/upload/user/avatar/" + value2.image_user} alt="" />
                                                </a>
                                                <div className="media-body">
                                                    <ul className="sinlge-post-meta">
                                                        <li><i className="fa fa-user" />Janis Gallagher</li>
                                                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                                    </ul>
                                                    <p>{value2.comment}</p>
                                                    <a className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                                                </div>
                                            </li>
                                        )
                                    }
                                })
                            }
                        </>
                    )
                }

            })
        }
    }
    return (
        <div className="response-area">
            <h2>3 RESPONSES</h2>
            <ul className="media-list">
                {renderData()}
            </ul>
        </div>
    )
}
export default ListComment;

