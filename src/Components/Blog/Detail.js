import { Component, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios"
import Comment from "./Comment";
import ListComment from "./ListComment";
import Rate from "./Rate";

function Detail() {
    let params = useParams()
    const [data, setData] = useState('')
    const [xx, setXx] = useState([])
    const [yy, setYy] = useState('')

    useEffect(() => {
        axios.get('http://localhost:8080/laravel/laravel/public/api/blog/detail/' + params.id)
            .then(res => {
                setData(res.data.data)
                setXx(res.data.data.comment)
            })
            .catch(error => console.log(error))
    }, [])
    function renderData() {
        if (Object.keys(data).length > 0) {
            return (
                <div className="blog-post-area">
                    <h2 className="title text-center">Latest From our Blog</h2>
                    <div className="single-blog-post">
                        <h3>{data.title}</h3>
                        <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user" /> Mac Doe</li>
                                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                            </ul>
                        </div>
                        <a href>
                            <img src={"http://localhost:8080/laravel/laravel/public/upload/Blog/image/" + data.image} alt="" />
                        </a>
                        <p>{data.description}</p>

                        <div className="pager-area">
                            <ul className="pager pull-right">
                                <li><a href="#">Pre</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            )
        }
    }
    function getCmt(x) {
        const cmt = xx.concat(x)
        setXx(cmt)
    }
    function getId(y) {
        setYy(y)
    }
    return (
        <>
            <div className="col-sm-9">
                {renderData()}
                <Rate />
                <div className="socials-share">
                    <a href><img src="images/blog/socials.png" alt="" /></a>
                </div>
                <ListComment data={xx} getId={getId} />
                <Comment getCmt={getCmt} yy={yy} />
            </div>
        </>
    )
}
export default Detail;
