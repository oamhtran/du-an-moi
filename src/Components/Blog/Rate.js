import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import StarRatings from 'react-star-ratings'

function Rate() {
    let params = useParams()
    const userData = JSON.parse(localStorage["Resdata"])
    const [rating, setRating] = useState(0)

    function changeRating(newRating, name) {
        let xx = localStorage.getItem("login")
        if (!xx) {
            alert("Vui lòng đăng nhập")
        } else {
            setRating(newRating)

            const formData = new FormData()
            formData.append('user_id', userData.Auth.id)
            formData.append('blog_id', params.id)
            formData.append('rate', newRating)

            let url = 'http://localhost:8080/laravel/laravel/public/api/blog/rate/' + params.id
            let accessToken = userData.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };

            axios.post(url, formData, config)
                .then((res) => {
                    // console.log(res)
                    if (res.data.errors) {
                        alert(res.data.errors)
                    }
                })
                .catch(error => console.log(error))
        }
    }
    useEffect(() => {
        axios.get("http://localhost:8080/laravel/laravel/public/api/blog/rate/" + params.id)
            .then(res => {
                // console.log(res.data.data)
                const xx = res.data.data
                let sum = 0
                if (xx.length > 0) {
                    xx.map((value, index) => {
                        sum = sum + value.rate
                    })
                    let avg = sum / xx.length
                    setRating(avg)
                    console.log(avg)
                }
            })
            .catch(error => console.log(error))
    }, [])

    return (
        <StarRatings
            rating={rating}
            starRatedColor="yellow"
            changeRating={changeRating}
            numberOfStars={6}
            name='rating'
        />
    )
}
export default Rate;




