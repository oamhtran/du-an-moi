import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router-dom";
function Comment(props) {
    const yy = props.yy
    const [textarea, setTextarea] = useState({
        commment: ""
    })
    let params = useParams()
    const handleComment = (e) => {
        const nameTextarea = e.target.name
        const value = e.target.value
        setTextarea(state => ({ ...state, [nameTextarea]: value }))
    }
    function PostComment() {
        let xx = localStorage.getItem("login")
        if (!xx) {
            alert("Vui long đăng nhập")
        } else {
            const userData = JSON.parse(localStorage["Resdata"])
            let url = 'http://localhost:8080/laravel/laravel/public/api/blog/comment/' + params.id
            let accessToken = userData.success.token
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };

            if (textarea) {
                const formData = new FormData()
                formData.append('id_blog', params.id)
                formData.append('id_user', userData.Auth.id)
                formData.append('id_comment', yy ? yy : 0)
                formData.append('comment', textarea.comment)
                formData.append('image_user', userData.Auth.avatar)
                formData.append('name_user', userData.Auth.name)

                axios.post(url, formData, config)
                    .then(res => {
                        console.log(res)
                        if (res.data.errors) {
                            alert(res.data.errors.comment)
                        } else {
                            props.getCmt(res.data.data)
                            alert("cmt thanh cong")
                        }
                    })
                    .catch(error => {
                        console.log(error)
                    }, [])
            }
        }
    }
    return (
        <>
            <div className="replay-box">
                <div className="row">
                    <div className="col-sm-12">
                        <h2>Leave a replay</h2>
                        <div className="text-area" id="idrelp">
                            <div className="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            <span>*</span>
                            <textarea name="comment" rows={11} defaultValue={""} onChange={handleComment} />
                            <a onClick={PostComment} className="btn btn-primary" href>post comment</a>
                        </div>
                    </div>
                </div>
            </div>{/*/Repaly Box*/}
        </>
    )
}

export default Comment;

